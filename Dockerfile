#FROM ubuntu:14.04
#MAINTAINER Vova
#RUN apt-get update
#RUN apt-get install -qy python3
#RUN apt-get install -qy python3-pip
#EXPOSE 80
#COPY app.py app.py
#COPY requirements.txt requirements.txt
#WORKDIR /
#CMD ls
#RUN pip3 install -r requirements.txt
#ENTRYPOINT ["python3"]
#CMD ["app.py"]
FROM ubuntu:latest
MAINTAINER Rajdeep Dua "dua_rajdeep@yahoo.com"
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
COPY requirements.txt requirements.txt
COPY app.py app.py
EXPOSE 80
WORKDIR /
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["app.py"]

